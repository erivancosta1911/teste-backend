package br.com.grao.testeBackend;

import java.math.BigDecimal;
import java.util.Set;

import br.com.grao.testeBackend.controller.CalculoController;
import br.com.grao.testeBackend.model.Calculo;

public class Main {

	public static void main(String[] args) {
		CalculoController calculoController = new CalculoController();
		Set<Calculo> resultado =
				calculoController.simular(new BigDecimal(100), 36);
		resultado.forEach((calculo) -> System.out.println(calculo));
	}
}
