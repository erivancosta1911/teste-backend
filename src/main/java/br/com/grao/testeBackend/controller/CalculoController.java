package br.com.grao.testeBackend.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;
import java.util.TreeSet;

import br.com.grao.testeBackend.model.Calculo;

public class CalculoController {
	private static final BigDecimal TAXA_SELIC_ANO_PORCENTAGEM =
			new BigDecimal(4.25);

	private static final BigDecimal QUANTIDADE_DIAS_UTEIS_ANO =
			new BigDecimal(252);

	private static final BigDecimal TAXA_SELIC_DIA_PORCENTAGEM =
			TAXA_SELIC_ANO_PORCENTAGEM.divide(QUANTIDADE_DIAS_UTEIS_ANO,
					10,
					RoundingMode.HALF_EVEN);

	private static final BigDecimal QUANTIDADE_DIAS_UTEIS_SEMANA =
			new BigDecimal(5);

	private static final BigDecimal TAXA_SELIC_SEMANA_PORCENTAGEM =
			TAXA_SELIC_DIA_PORCENTAGEM.multiply(QUANTIDADE_DIAS_UTEIS_SEMANA);

	private static final BigDecimal TAXA_SELIC_SEMANA_DECIMAL =
			TAXA_SELIC_SEMANA_PORCENTAGEM
					.divide(new BigDecimal(100), 10, RoundingMode.HALF_EVEN);

	public Set<Calculo> simular(BigDecimal valorInicalInvestido,
			int numeroDeSemanas) {
		Set<Calculo> calculos = new TreeSet<Calculo>();

		BigDecimal valorPoupado = valorInicalInvestido;
		BigDecimal valorInvestido = valorInicalInvestido;

		for (int semana = 1; semana <= numeroDeSemanas; semana++) {
			valorInvestido = valorPoupado;

			Calculo calculo = new Calculo(semana, valorPoupado);

			valorPoupado = BigDecimal.ONE.add(TAXA_SELIC_SEMANA_DECIMAL)
					.pow(semana)
					.multiply(valorInvestido)
					.setScale(2, RoundingMode.HALF_EVEN);

			BigDecimal valorRendido = valorPoupado.subtract(valorInvestido);

			calculo.setValorPoupado(valorPoupado);
			calculo.setValorRendido(valorRendido);

			valorPoupado = valorPoupado.add(valorInicalInvestido);

			calculos.add(calculo);
		}

		return calculos;
	}
}
