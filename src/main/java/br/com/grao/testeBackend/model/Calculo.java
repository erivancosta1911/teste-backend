package br.com.grao.testeBackend.model;

import java.math.BigDecimal;

public class Calculo implements Comparable<Calculo> {
	private Integer semana;
	private BigDecimal valorInvestido;
	private BigDecimal valorPoupado;
	private BigDecimal valorRendido;

	public Calculo(Integer semana, BigDecimal valorInvestido) {
		super();
		this.semana = semana;
		this.valorInvestido = valorInvestido;
	}

	public Calculo(Integer semana,
			BigDecimal valorInvestido,
			BigDecimal valorPoupado,
			BigDecimal valorRendido) {
		super();
		this.semana = semana;
		this.valorInvestido = valorInvestido;
		this.valorPoupado = valorPoupado;
		this.valorRendido = valorRendido;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public BigDecimal getValorInvestido() {
		return valorInvestido;
	}

	public void setValorInvestido(BigDecimal valorInvestido) {
		this.valorInvestido = valorInvestido;
	}

	public BigDecimal getValorPoupado() {
		return valorPoupado;
	}

	public void setValorPoupado(BigDecimal valorPoupado) {
		this.valorPoupado = valorPoupado;
	}

	public BigDecimal getValorRendido() {
		return valorRendido;
	}

	public void setValorRendido(BigDecimal valorRendido) {
		this.valorRendido = valorRendido;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((semana == null) ? 0 : semana.hashCode());
		result = prime * result
				+ ((valorInvestido == null) ? 0 : valorInvestido.hashCode());
		result = prime * result
				+ ((valorPoupado == null) ? 0 : valorPoupado.hashCode());
		result = prime * result
				+ ((valorRendido == null) ? 0 : valorRendido.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Calculo other = (Calculo) obj;
		if (semana == null) {
			if (other.semana != null)
				return false;
		} else if (!semana.equals(other.semana))
			return false;
		if (valorInvestido == null) {
			if (other.valorInvestido != null)
				return false;
		} else if (!valorInvestido.equals(other.valorInvestido))
			return false;
		if (valorPoupado == null) {
			if (other.valorPoupado != null)
				return false;
		} else if (!valorPoupado.equals(other.valorPoupado))
			return false;
		if (valorRendido == null) {
			if (other.valorRendido != null)
				return false;
		} else if (!valorRendido.equals(other.valorRendido))
			return false;
		return true;
	}

	@Override
	public int compareTo(Calculo o) {
		if (this.semana < o.getSemana()) {
			return -1;
		}
		if (this.semana > o.getSemana()) {
			return 1;
		}
		return 0;
	}

	@Override
	public String toString() {
		return semana + "ª semana "
				+ "investiu R$ "
				+ valorInvestido
				+ ", poupo R$ "
				+ valorPoupado
				+ ", com redimento de R$ "
				+ valorRendido;
	}

}
