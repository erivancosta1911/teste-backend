FROM maven:3.6.3-openjdk-11 as maven
COPY ./pom.xml ./pom.xml
COPY ./src ./src
RUN mvn dependency:go-offline -B
RUN mvn package
FROM openjdk:11-jre
WORKDIR /erivan
COPY --from=maven target/teste-backend-*.jar ./erivan/teste-backend.jar
CMD ["java", "-jar", "./erivan/teste-backend.jar"]